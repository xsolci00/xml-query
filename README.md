| Dokumentace úlohy XQR:   | XML Query v PHP 5 do IPP 2016/2017    |
 | ------| ------------ |
 | jméno | Vít Solčík  |
 | login | xsolci00     |


### Obecný popis skriptu: 
Skript psaný v jazyce PHP má jako vstup XML soubor nad kterým provede požadavek (query) určený gramatikou popsanou v zadání projektu (patrná podobnost s SQL příkazem SELECT). Výstupem je opět XML soubor (až na menší rozdílnosti při absenci root elementu) odpovídající požadavku (query).


### Vstupní argumenty:
Argumenty jsou zpracovány funkcí arguments(array $argv,int $argc). Jejich validita (tzn. korektní kombinace a správný zápis) je nejdříve ověřena několika regularnímy výrazy a následně jsou zpracovány funkcí getopt(string $shortopts,array $longopts) která načte samotný obsah argumentů. Query požadavek je kvůli své složitosti oproti ostatním argumentům zpracován a rozdělen na logické celky v samostatné funkci parse_query(string $query), opět za  využití několika regulárních výrazů, které zároveň fungují jako syntaktický analyzátor pro jazyk určený gramatikou.


### Zpracování XML vstupu:
Zpracování query probíhá ve funkci apply_query(array $query, DOMDocument $xml). Celý vstupni XML dokument je načten pomocí funkcí fopen( string $filename , string $mode) a fread( resource $handle , int $length ). Dále je obsah souboru převeden na objekt DOMDocument třídy. 


### Vykonání query:
Pro vyhledávání v dokumentu jsou využívány metody DOMDocument třídy,  zejména pak metoda getElementsByTagName(string $name) a hasAttribute(string $atribute). Naprostá většina vyhledávání je tvořena několika vnořenými foreach cykly podmíněnými zadaným dotazem.


### Výstup:
Pro výstup je za běhu skriptu postupně vytvořen nový objekt DOMDocument třídy, který je následně převeden na výstupní řetězec. Který je případně doplněn o hlavičku ( v závislosti na argumentu -n) a párový kořenový element (argument --root=element) . Tento řetězec je předán na výstup, což je buď soubor určený argumentem --outpu=<file> nebo standardní výstup.