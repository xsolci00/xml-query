#!/usr/bin/php

<?php

/**
 * @file    xqr.php
 * @author  Vit Solcik
 * @date:   2017-08-01
 */

//Notice while testing undefined offsets at array
error_reporting(0);

// function to handle arguments
$arguments = arguments($argv, $argc);

// loading input
if (isset($arguments["input"]))
{
    $xml = load_input($arguments["input"]);
}
else
{
    $xml = file_get_contents("php://stdin");
}


if (isset($arguments["query"]))
{
    $query = parse_query($arguments["query"]);
    $doc = new DOMDocument();
    $doc->loadXML($xml);
    $xml_out = apply_query($query, $doc);
}

$out = $xml_out->saveHTML();


if (isset($arguments["root"]) && (empty($xml_out->textContent)))
{
    $out = ("<" . $arguments["root"] . "/>\n");
}
else
if (isset($arguments["root"])) {
    $out = ("<" . $arguments["root"] . ">\n" . "$out" ."</" . $arguments["root"] . ">" );
}


if (!isset($arguments["no_header"])) {
     $out = "<?xml version=\"1.0\" encoding=\"utf-8\"?>" . "$out"; 
}


    

// output the result
if (isset($arguments["output"]))
{
    //$out->save($arguments["output"]);
    file_put_contents($arguments["output"], $out);
}
else
{
    //$out->save("php://stdin");
    print($out);
}



/**
 * { function_description }
 *
 * @param      <type>  $query  The query
 * @param      <type>  $xml    The xml
 */
function apply_query($query, $xml)
{

    $outputXmlData = new DOMDocument();
    $outputXmlData->formatOutput = true;
    $limit_count = 0;
    $find = FALSE;


    if ( $query['WHERE'] !== FALSE)
    {
        if (!empty($where = $query['WHERE']['ELEMENT']))
            $where = $query['WHERE']['ELEMENT'];
            

        if ($query['WHERE']['LITERAL'] == "STRING")
            $contains = $query['WHERE']['STRING'];
        elseif ($query['WHERE']['LITERAL'] == "NUMBER")
            $contains = $query['WHERE']['SPLIT'][9];

        $operator = $query['WHERE']['SPLIT'][8];
    }


    if ($query['LIMIT'] !== FALSE)
        $limit = $query['LIMIT']['RESULT'][2]; 
    else
        $limit = 42;


    $select = $query['SELECT']['RESULT'][2];
    
    $from = $query['FROM']['RESULT'][2];


    if (empty($from)) {
        $from=$xml->documentElement->nodeName;
    }

    if (!empty($query['FROM']['RESULT'][3]))
        $atribute = $query['FROM']['RESULT'][3];

    if ($from != "ROOT")
    {

        $elements_from = $xml->getElementsByTagName($from);
//        else
//            $elements_from = $xml->getElementsByTagName($atribute);

        // FROM element
        if (preg_match("/^[a-zA-Z][a-zA-Z0-9]*$/", $query['FROM']['RESULT'][0]))
            $element_from = $elements_from->item(0);

        //FROM element.atribut
        else if (!(empty($query['FROM']['RESULT'][2])) && !(empty($atribute)))
        {
            $i = 0;
            foreach ($elements_from as $element_from)
            { 
                if ($element_from->hasAttribute($atribute))
                {
                    $element_from = $elements_from->item($i);
                    $find = TRUE;
                    break;
                }
                $i++;
            }
            if($find != TRUE)
                return $outputXmlData;
        }   
        else
        {

            $found_atribut = atributes($atribute, $xml);
        
            $elements_from = $xml->getElementsByTagName($found_atribut);


            $i = 0;
            foreach ($elements_from as $element_from)
            {   

                if ($element_from->hasAttribute($atribute))
                {
                    $element_from = $elements_from->item($i);
                    $find = TRUE;
                    break;
                }
                $i++;
            }

            if($find != TRUE)
            {                
                return $outputXmlData;
            } 

        }    
    }// if ROOT


    if ($from == "ROOT")
        $elements_select = $xml->getElementsByTagName($select);
    else 
        $elements_select = $element_from->getElementsByTagName($select);



    foreach ($elements_select as $element_select)
    {
        // SELECT elem FROM elem WHERE xxxx
        if ( $query['WHERE'] !== FALSE)
        {
            // .attribute
            if ($query['WHERE']['FLAG'] == 'ATRIBUTE' || ($select == $where))
                $elements_where = array(0);
            else
                $elements_where = $element_select->getElementsByTagName($where);    


            foreach ($elements_where as $element_where)
            {

                //.atribute
                if ($query['WHERE']['FLAG'] == 'ATRIBUTE' || ($select == $where))
                    $element_where = $element_select;
                

                if ($query["WHERE"]["FLAG"] == "ELEMENT")
                    $tested = $element_where->nodeValue;
                else
                    $tested = $element_where->getAttribute($query['WHERE']['ATRIBUTE']);


                if ($limit_count < $limit )
                {
                    if ($query['WHERE']['LITERAL'] == "STRING")
                    {
                        if ($operator == "CONTAINS")
                        {
                            // **** CONTAINS ****
                            if ((strstr($tested, $contains) != false )and
                                ($query['WHERE']['NOT'] == FALSE)) 
                            {
                                $newElement = $outputXmlData->importNode($element_select, true);
                                $outputXmlData->appendChild($newElement);
                            }
                            elseif  ((strstr($tested, $contains) == false ) and
                                $query['WHERE']['NOT'] == TRUE)
                            {
                                $newElement = $outputXmlData->importNode($element_select, true);
                                $outputXmlData->appendChild($newElement);
                            }
                            // **** CONTAINS ****
                        }                        
                        if ($operator == "=")
                        {
                            // **** = ****
                            if ((strcmp($tested, $contains) == 0 )and
                                ($query['WHERE']['NOT'] == FALSE)) 
                            {
                                $newElement = $outputXmlData->importNode($element_select, true);
                                $outputXmlData->appendChild($newElement);
                            }
                            elseif  ((strcmp($tested, $contains) != 0 ) and
                                $query['WHERE']['NOT'] == TRUE)
                            {
                                $newElement = $outputXmlData->importNode($element_select, true);
                                $outputXmlData->appendChild($newElement);
                            }
                            // **** = ****
                        }                        
                        if ($operator == "<")
                        {
                            // **** < ****
                            if ((strcmp($tested, $contains) == -1 )and
                                ($query['WHERE']['NOT'] == FALSE)) 
                            {
                                $newElement = $outputXmlData->importNode($element_select, true);
                                $outputXmlData->appendChild($newElement);
                            }
                            elseif  ((strcmp($tested, $contains) != -1 ) and
                                $query['WHERE']['NOT'] == TRUE)
                            {
                                $newElement = $outputXmlData->importNode($element_select, true);
                                $outputXmlData->appendChild($newElement);
                            }
                            // **** < ****
                        }
                        if ($operator == ">")
                        {
                            // **** < ****
                            if ((strcmp($tested, $contains) == 1 )and
                                ($query['WHERE']['NOT'] == FALSE)) 
                            {
                                $newElement = $outputXmlData->importNode($element_select, true);
                                $outputXmlData->appendChild($newElement);
                            }
                            elseif  ((strcmp($tested, $contains) != 1 ) and
                                $query['WHERE']['NOT'] == TRUE)
                            {
                                $newElement = $outputXmlData->importNode($element_select, true);
                                $outputXmlData->appendChild($newElement);
                            }
                            // **** < ****
                        }
                    }
                    elseif ($query['WHERE']['LITERAL'] == "NUMBER")
                    {
                        if ($operator == "=")
                        {
                            // **** = ****
                            if (($tested ==  $contains) and
                                ($query['WHERE']['NOT'] == FALSE)) 
                            {
                                $newElement = $outputXmlData->importNode($element_select, true);
                                $outputXmlData->appendChild($newElement);
                            }
                            elseif  (($tested !=  $contains) and
                                $query['WHERE']['NOT'] == TRUE)
                            {
                                $newElement = $outputXmlData->importNode($element_select, true);
                                $outputXmlData->appendChild($newElement);
                            }
                            // **** = ****
                        }
                        if ($operator == "<")
                        {
                            // **** < ****
                            if (($tested <  $contains) and
                                ($query['WHERE']['NOT'] == FALSE)) 
                            {
                                $newElement = $outputXmlData->importNode($element_select, true);
                                $outputXmlData->appendChild($newElement);
                            }
                            elseif  (($tested >  $contains) and
                                $query['WHERE']['NOT'] == TRUE)
                            {
                                $newElement = $outputXmlData->importNode($element_select, true);
                                $outputXmlData->appendChild($newElement);
                            }
                            // **** < ****
                        }
                        if ($operator == ">")
                        {
                            // **** < ****
                            if (($tested > $contains) and
                                ($query['WHERE']['NOT'] == FALSE)) 
                            {
                                $newElement = $outputXmlData->importNode($element_select, true);
                                $outputXmlData->appendChild($newElement);
                            }
                            elseif  (($tested <  $contains) and
                                $query['WHERE']['NOT'] == TRUE)
                            {
                                $newElement = $outputXmlData->importNode($element_select, true);
                                $outputXmlData->appendChild($newElement);
                            }
                            // **** < ****
                        }
                    }

                    if ($query['LIMIT'] !== FALSE)
                    {
                        $limit_count  += 1;
                    }
                }
            }
        }
        else //SELECT elem FROM elem
        {

            if ($limit_count < $limit)
            {
                $newElement = $outputXmlData->importNode($element_select, true);
                $outputXmlData->appendChild($newElement);
                if ($query['LIMIT'] !== FALSE)
                {
                    $limit_count  += 1;
                }
            }
        }
    }



    
    //$outputXmlData->save("test.xml");
    return $outputXmlData;
}


/**
 * { function_description }
 *
 * @param      <type>  $query  The query
 */
function parse_query($query)
{
    $query_syntax = "/\s*(SELECT\s*.*\s*FROM\s*.*\s*(WHERE)?\s*.*\s*(LIMIT)?)\s*.*\s*/";
        
    preg_match($query_syntax, $query, $matches);
    if (empty($matches))
    {
        error_exit("Wrong query\n", 80);
    }

    //SELECT
    //$query_part[0]['STRING'] = "SELECT";
    $query_part['SELECT']['REGEX'] = "/(?<=SELECT[\r\n\t\f\v ])(\s*)([a-zA-Z0-9_]+)/";
    $query_part['SELECT']['RESULT'] = "NULL";
    preg_match($query_part['SELECT']['REGEX'], $query, $query_part['SELECT']['RESULT']);

    if (empty($query_part['SELECT']['RESULT'][0]))
    {
        error_exit("Wrong query (probably SELECT part)\n", 80);
    }


    //FROM
    //$query_part[1]['STRING'] = "FROM";
    $query_part['FROM']['REGEX'] = "/(?<=FROM[\r\n\t\f\v ])(\s*)([a-zA-Z0-9_]+)?(?:\.?([a-zA-Z0-9_]+)?)/";
    preg_match($query_part['FROM']['REGEX'], $query, $query_part['FROM']['RESULT']);
    if (empty($query_part['FROM']['RESULT'][0]))
    {
        error_exit("Wrong query (probably FROM part)\n", 80);
    }



    //WHERE
    if (strpos($query, "WHERE") == TRUE)
    {
        //$query_part[2]['WHERE'] = "WHERE";
        
        if (strpos($query, "LIMIT") == TRUE)
        {
            $query_part['WHERE']['REGEX'] = "/(?<=WHERE[\r\n\t\f\v ]).*(?=LIMIT)/";
        }
        else
        {
            $query_part['WHERE']['REGEX'] = "/(?<=WHERE[\r\n\t\f\v ]).*/";
        }
        preg_match($query_part['WHERE']['REGEX'], $query, $query_part['WHERE']['RESULT']);

        if (empty($query_part['WHERE']['RESULT'][0]) and $query_part['WHERE']['RESULT'][0] != "0")
        {
            error_exit("Wrong query (probably WHERE part)\n", 80);
        }
    }

    //LIMIT
    if (strpos($query, "LIMIT") == TRUE)
    {
        $query_part['LIMIT']['STRING'] = "LIMIT";
        //$query_part['LIMIT']['REGEX1'] = "/(?<=LIMIT[\r\n\t\f\v ])\".*\"/";
        $query_part['LIMIT']['REGEX2'] = "/(?<=LIMIT[\r\n\t\f\v ])(\s*)([0-9]*)(\s*)$/";
        
        //preg_match($query_part['LIMIT']['REGEX1'], $query, $query_part['LIMIT']['RESULT']);
    
        preg_match($query_part['LIMIT']['REGEX2'], $query, $query_part['LIMIT']['RESULT']);

        if (empty($query_part['LIMIT']['RESULT'][0]) and $query_part['LIMIT']['RESULT'][0] != "0")
        {
            error_exit("Wrong query (probably LIMIT part)\n", 80);
        }
    }
    else
    {
        $query_part['LIMIT'] = FALSE;
    }
/*
    if (empty($query_part['FROM']['RESULT'][0]) == FALSE)
    {
       $query_split['FROM'] = preg_split("/[.]/", $query_part['FROM']['RESULT'][0]);
    }
*/

    // WHEN Regex
    //$split_where_expr ="/^((?:NOT\s+)*)[a-zA-Z]([a-zA-Z0-9_]+)?(?:\.?(["
    //                ."a-zA-Z0-9_]+)?)\s*(([<>=]|CONTAINS)"
    //                ."\s*((((-|\+)?\d+))|\"[a-zA-Z0-9_ ]*\"))/";


    if (strpos($query, "WHERE") == TRUE)
    {
        $split_where_expr="/^\s*((?:NOT\s*)*)\s*((\.(\w*)?)|([a-zA-Z]*)?([a-zA-Z0-9.]+))\s*(([<>=]|CONTAINS)\s*((((-|\+)?\d+))|\".*\"))\s*$/";
        preg_match($split_where_expr, $query_part['WHERE']['RESULT'][0],$query_part['WHERE']['SPLIT']);
        if (empty($query_part['WHERE']['SPLIT']))
        {
            error_exit("Wrong query (probably WHERE part)\n", 80);
        }

        preg_match("/^\w*$/",$query_part['WHERE']['SPLIT'][2],$element);

        if (!empty($element))
        {
            $query_part['WHERE']['ELEMENT'] = $element[0];
        }

        preg_match("/^[a-zA-Z.]*$/",$query_part['WHERE']['SPLIT'][2],$element2);


        if (!(empty($element2)) && !(empty($query_part['WHERE']['SPLIT'][5]))
            && empty($query_part['WHERE']['ELEMENT'])
            )
        {
            $query_part['WHERE']['ELEMENT'] = $query_part['WHERE']['SPLIT'][5];
        }

        preg_match("/^\"(.*)\"$/",$query_part['WHERE']['SPLIT'][9],$quoted_string);

        if (!empty($quoted_string))
        {
            $query_part['WHERE']['STRING'] = $quoted_string[1];
            $query_part['WHERE']['LITERAL'] = "STRING";
        }
        else
        {
            $query_part['WHERE']['LITERAL'] = "NUMBER";
        }


        preg_match("/^\.(\w*)$/",$query_part['WHERE']['SPLIT'][6],$atribute);

        if (!empty($atribute))
        {
            $query_part['WHERE']['ATRIBUTE'] = $atribute[1];
        }

        if (!empty($query_part['WHERE']['SPLIT'][4]))
        {
            $query_part['WHERE']['ATRIBUTE'] = $query_part['WHERE']['SPLIT'][4];
        }



        $not_count = str_word_count($query_part['WHERE']['SPLIT'][1]);
        if (($not_count % 2) == 1)
        {
            $query_part['WHERE']['NOT'] = TRUE;   
        }
        else
        {
            $query_part['WHERE']['NOT'] = FALSE;   

        }
    
        if (!empty($query_part['WHERE']['ELEMENT']) && !empty($query_part['WHERE']['ATRIBUTE'])) {
            $query_part['WHERE']['FLAG'] = "BOTH";
        }
        elseif (empty($query_part['WHERE']['ELEMENT']) && !empty($query_part['WHERE']['ATRIBUTE'])) {
            $query_part['WHERE']['FLAG'] = "ATRIBUTE";
        }
        elseif (!empty($query_part['WHERE']['ELEMENT']) && empty($query_part['WHERE']['ATRIBUTE'])) {
            $query_part['WHERE']['FLAG'] = "ELEMENT";
        }
    }
    else
    {
        $query_part['WHERE'] = FALSE;
    }
    

    if (($operator = $query_part['WHERE']['SPLIT'][8] == "CONTAINS") and
        $query_part['WHERE']['LITERAL'] == "NUMBER")
    {
        error_exit("Wrong query (probably WHERE part)\n", 80);
    }


    return $query_part;

}


/**
 * Loads a file.
 *
 * @param      <type>  $file   The file
 *
 * @return     <type>  ( description_of_the_return_value )
 */
function load_input($file)
{
    if (is_readable($file))
    {
        $fileDescriptor = fopen($file, "r");
        $content = fread($fileDescriptor, filesize($file));
        fclose($fileDescriptor);
        return $content;
    } 
    else 
    {
        error_exit("Can not open input XML file\n", 2);
    }
}

/**
 * { function_description }
 *
 * @param      <type>  $message    The message
 * @param      <type>  $exitValue  The exit value
 */
function error_exit($message, $exitValue)
{
    file_put_contents("php://stderr", $message);
    exit($exitValue);
}

/**
 * { function_description }
 */
function help_message()
{
    echo(
        "--help\t\t\t\tprint help\n".
        "--input=<filename>\t\tinput file in XML format\n".
        "--output=<filename>\t\toutput file in XML format containing qeury data\n".
        "--query='request'\t\t\tquery request respond to defined language (viz docs)\n".
        "--qf=filename\t\t\tquery request in external file\n".
        "\t\t\t\tcombine with --query in not allowed\n".
        "-n\t\t\t\tdo not generate XML header in output file\n".
        "--root=element\t\t\tname of root element with results\n"
        );
}



/**
 * { function_description }
 *
 * @param      <type>   $argv   The argv
 * @param      integer  $argc   The argc
 *
 * @return     array    ( description_of_the_return_value )
 */
function arguments($argv, $argc)
{
    $paramRegex[0] = "/^.*\.php$/";
    $paramRegex[1] = "/^(--help)$/";
    $paramRegex[2] = "/^(-n)$/";
    $paramRegex[3] = "/^--input=(.*)$/";
    $paramRegex[4] = "/^--output=(.*)$/";
    $paramRegex[5] = "/^--query=(SELECT.*)$/";
    $paramRegex[6] = "/^--qf=(.*)$/";
    $paramRegex[7] = "/^--root=(.*)$/";


    foreach ($argv as $argvParam) {
        for ($i = 0; ($i <= count($paramRegex) and empty($matches)); $i++)
        {
            preg_match($paramRegex[$i], $argvParam, $matches);
        }
        
        if (empty($matches[0]))
        {
            error_exit("Wrong parametres run with --help\n", 1);
        }
    }


    $shortopts = "n";
    $longopts = array(
        "help",
        "input:",
        "output:",
        "query:",
        "qf:",
        "root::",
    );

    $opts = getopt($shortopts, $longopts);
    $arguments = array();


    if ( $argc -1 != count($opts))
    {
        error_exit("Wrong parametres run with --help\n", 1);
    }
    if (array_key_exists("help", $opts) and ( $argc != 2))
    {
        error_exit("Wrong parametres run with --help\n", 1);
    }
    if (isset($opts["query"]) and isset($opts["qf"]))
    {
        error_exit("Wrong parametres run with --help\n", 1);
    }



    if (array_key_exists("help", $opts))
    {
        help_message();
        error_exit("\n", 0);
    }
    if (array_key_exists("n", $opts))
    {
        $arguments["no_header"] = TRUE;
    }
    if (isset($opts["output"]))
    {
        $arguments["output"] = $opts["output"];
    }
    if (isset($opts["query"]))
    {
        $arguments["query"] = $opts["query"];
    }
    if (isset($opts["root"]))
    {
        $arguments["root"] = $opts["root"];
    }
 
    if (isset($opts["input"]))
    {
        if (file_exists($opts["input"]) == TRUE)
        {
            $arguments["input"] = $opts["input"];
        }
        else
        {
            error_exit("Input XML file does not exist\n", 2);
        }
    }

    if (isset($opts["qf"]))
    {
        if (file_exists($opts["qf"]) == TRUE)
        {
            $arguments["query"] = load_input($opts["qf"]);
        }
        else
        {
            error_exit("Input query file does not exist\n", 2);
        }
    }

    //var_dump($arguments);
    return $arguments;
}


/**
 * { function_description }
 *
 * @param      <type>  $atribute  The atribute
 * @param      <type>  $xml       The xml
 *
 * @return     <type>  ( description_of_the_return_value )
 */
function atributes($atribute, $xml)
{
            
    foreach($xml->childNodes as $first_loop)
    { 
        if ($first_loop->nodeName != "#text")
        {

            if ($first_loop->hasAttribute($atribute))
            {
                $result = $first_loop->nodeName;
                return $result;
            }
        }

        if ($first_loop->hasChildNodes())
        {
            
            foreach($first_loop->childNodes as $second_loop)
            {
                
                if ($second_loop->nodeName != "#text")
                {

                    if ($second_loop->hasAttribute($atribute))
                    {
                        $result = $second_loop->nodeName;
                        return $result;
                    }
                }
                
                if ($second_loop->hasChildNodes())
                {

                    foreach($second_loop->childNodes as $third_loop)
                    {

                        if ($third_loop->nodeName != "#text")
                        {
                            if ($third_loop->hasAttribute($atribute))
                            {
                                $result = $third_loop->nodeName;
                                return $result;
                            }
                        }

                        if ($third_loop->hasChildNodes())
                        {

                            foreach($third_loop->childNodes as $fourth_loop)
                            {

                                if ($fourth_loop->nodeName != "#text")
                                {
                                    if ($fourth_loop->hasAttribute($atribute))
                                    {
                                        $result = $fourth_loop->nodeName;
                                        return $result;
                                    }
                                }
                            }//4
                        }
                    }//3
                }
            }//3
        }
    }//1  
}



?>